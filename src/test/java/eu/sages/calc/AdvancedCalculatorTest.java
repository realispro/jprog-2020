package eu.sages.calc;

import org.junit.Assert;
import org.junit.Test;

public class AdvancedCalculatorTest {

    @Test
    public void testDivisionByZero(){
        AdvancedCalculator calculator = new AdvancedCalculator();
        try {
            calculator.divide(0);
            Assert.fail("should be interrupted by an exception");
        } catch (CalculatorException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testPowerZero(){
        AdvancedCalculator calculator = new AdvancedCalculator(5);
        Assert.assertEquals(1, calculator.power(0), 0);
    }

}
