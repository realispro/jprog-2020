package eu.sages.zoo;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class CamelTest {

    @Test
    public void testTransport() {
        Camel camel = new Camel(100, "Ala");
        try {
            camel.transport("Ala's Passenger");
        }catch (Exception e){
            Assert.fail("failed because of exception " + e.getMessage());
        }
    }
}