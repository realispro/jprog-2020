package eu.sages;

import eu.sages.calc.Calculator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

// UpperCamelCase
public class HelloApp {

    //psvm
    public static void main(String[] args){
        System.out.println("Hello everyone!"); // sout

        // byte, short, int, long
        long age = Long.MAX_VALUE;
        age  = 126456635468L;
        System.out.println("age = " + age);
        
        // float, double
        float distance = 23.4567F;
        System.out.println("distance = " + distance);

        WeekDay weekDay = WeekDay.WEDNESDAY;
        System.out.println("weekDay = " + weekDay.name());

        boolean weekend= weekDay.weekend;
        System.out.println("weekend = " + weekend);

        char c = '\t';
        System.out.println("(short)c = " + (short)c);

        String[] students = /*new String[]*/{"Agnieszka", "Pawel", "Bartosz", "Krzysztof"};

        System.out.println("students group size: " + students.length);

        for(int i=0; i<students.length; i++){
            System.out.println("student[" + i + "] " + students[i]);
        }

        for(String student : students){
            System.out.println("student: " + student);
        }

        // int, short, byte, String, enum
        switch (weekDay){
            case FRIDAY:
            case SATURDAY:
            case SUNDAY:
                System.out.println("weekend!");
                break;
            default:
                System.out.println("not a weekend :(");

        }

        // multiplication table 10x10
        /*int size;
        if(args.length>0){
            size = Integer.parseInt(args[0]);
        } else {
            size = 15;
        }*/
        // ternary operator
        int size = args.length>0 ? Integer.parseInt(args[0]) : 15;

        System.out.println("calculating multiplication table of size: " + size);
        // TODO 1. define data structure
        int[][] results = new int[size][size];

        // TODO 2. fill structure with proper values
        boolean stop = false;
        for(int x=1; x<=size&&!stop; x++){
            for(int y=1; y<=size; y++){
                if(y==13){
                    stop = true;
                    break;
                }
                results[x-1][y-1] = Calculator.multiply(x,y);
            }
        }

        // TODO 3. present values from structure
        for(int[] row : results){
            for(int value : row){
                System.out.print(value+"\t");
            }
            System.out.println();
        }
        
        String s = join(new String[]{"a", "b", "c", "d"});
        System.out.println("s = " + s);

        String text = "Jan Kowalski, 67-890 ul. Sienkiewicza 2/4, 12-345 Wolka Wielka";
        String patternText = "\\d\\d-\\d\\d\\d";  // \d\d-\d\d\d

        Pattern pattern = Pattern.compile(patternText);
        Matcher matcher = pattern.matcher(text);
        while(matcher.find()){
            System.out.println( matcher.start() + " " + matcher.end() + " " + matcher.group());
        }

    }
    
    public static String join(String[] strings){
        StringBuilder builder = new StringBuilder();
        for(String s : strings){
            builder.append(s);
        }
        return builder.toString();
    }

    public static int add(int x, int y){
        return x+y;
    }


}
