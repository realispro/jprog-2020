package eu.sages.chat;

import eu.sages.concurrency.ThreadNamePrefixPrintStream;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ChatClientApp {

    public static void main(String[] args) {
        System.setOut(new ThreadNamePrefixPrintStream(System.out));
        System.out.println("ChatClientApp.main");

        ExecutorService es = Executors.newFixedThreadPool(2);
        try {
            Socket socket = new Socket("127.0.0.1", 5555);
            System.out.println("connected.");

            es.execute(new MessageWriter(socket.getOutputStream()));
            es.execute(new MessageReader(socket.getInputStream()));

            System.out.println("done.");
        } catch (IOException e) {
            e.printStackTrace();
        }

        es.shutdown();

    }
}
