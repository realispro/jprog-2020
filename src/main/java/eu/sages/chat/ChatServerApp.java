package eu.sages.chat;

import eu.sages.concurrency.ThreadNamePrefixPrintStream;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ChatServerApp {

    public static void main(String[] args) {
        System.setOut(new ThreadNamePrefixPrintStream(System.out));
        System.out.println("ChatServerApp.main");

        ExecutorService es = Executors.newFixedThreadPool(2);

        try {
            ServerSocket serverSocket = new ServerSocket(5555);
            System.out.println("listening on 5555...");
            Socket socket = serverSocket.accept();
            System.out.println("client connected.");

            es.execute(new MessageWriter(socket.getOutputStream()));
            es.execute(new MessageReader(socket.getInputStream()));

            System.out.println("done.");

        } catch (IOException e) {
            e.printStackTrace();
        }
        es.shutdown();

    }
}
