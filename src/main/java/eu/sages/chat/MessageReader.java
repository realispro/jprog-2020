package eu.sages.chat;

import java.io.*;

public class MessageReader implements Runnable{

    private BufferedReader br;

    public MessageReader(InputStream is){
        br = new BufferedReader(new InputStreamReader(is));
    }

    @Override
    public void run() {
        System.out.println("reader started.");
        try {
            String msg;
            while ((msg = br.readLine()) != null) {
                System.out.println("[in] " + msg);
            }
            System.out.println("reader closed.");
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
