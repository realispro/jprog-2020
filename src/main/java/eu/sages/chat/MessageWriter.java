package eu.sages.chat;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Scanner;

public class MessageWriter implements Runnable {

    private BufferedWriter bw;

    public MessageWriter(OutputStream os){
        bw = new BufferedWriter(new OutputStreamWriter(os));
    }

    @Override
    public void run() {

        System.out.println("writer started.");

        Scanner scanner = new Scanner(System.in);
        try(BufferedWriter bufferedWriter = bw) {
            while(true) {
                String msg = scanner.nextLine();
                bufferedWriter.write(msg + "\n");
                bufferedWriter.flush();
            }
        }catch (IOException e){
            e.printStackTrace();
        }

        System.out.println("writer closed.");
    }
}
