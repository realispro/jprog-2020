package eu.sages.javafx.guess;

import java.util.concurrent.ThreadLocalRandom;


public class SecretNumber {

    private int secret;

    public SecretNumber() {
        reset();
    }

    public void reset(){
        secret = ThreadLocalRandom.current().nextInt(100);
    }


    public int guess(int guess){
        return secret - guess;
    }
}
