package eu.sages.javafx.nameday;

import javafx.application.Application;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;


public class NameDayApp extends Application {

    private NameDayInfo nameDayInfo = new NameDayInfo();

    private TextField nameField;

    @Override
    public void start(Stage primaryStage) throws Exception {

        primaryStage.setTitle("Name Day");

        GridPane pane = new GridPane();
        pane.setAlignment(Pos.CENTER);
        pane.setHgap(10);
        pane.setVgap(10);
        pane.setPadding(new Insets(25, 25, 25, 25));

        ColumnConstraints column1 = new ColumnConstraints();
        column1.setHalignment(HPos.CENTER);
        pane.getColumnConstraints().add(column1);

        Scene scene = new Scene(pane, 400, 500);

        Image i = new Image("tort.jpg");
        ImageView imgView = new ImageView(i);
        pane.add(imgView, 0, 0);

        Text sceneTitle = new Text("Is it your name day today?");
        pane.add(sceneTitle, 0, 1);

        Label nameLabel = new Label("Tell me your name");
        pane.add(nameLabel, 0, 2);

        nameField = new TextField();
        pane.add(nameField, 0, 3);

        Button button = new Button("Check !");
        pane.add(button, 0, 4);

        button.setOnAction(event -> {

                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Name Day Check");
                alert.setHeaderText("Checking Name Day");
                String message = nameDayInfo.isYourNameDay(nameField.getText()) ?
                                "Happy Name Day, " + nameField.getText() + " !":
                                "You must wait for your name day, " + nameField.getText() + " !";
                alert.setContentText(message);
                alert.showAndWait();
            }
        );

        WebView browser = new WebView();
        WebEngine webEngine = browser.getEngine();
        pane.add(browser, 0, 5);
        webEngine.load("http://www.namedaycalendar.com/poland");


        String css = getClass().getResource("/nameday.css").toExternalForm();
        scene.getStylesheets().clear();
        scene.getStylesheets().add(css);

        primaryStage.setScene(scene);
        primaryStage.show();
    }

}
