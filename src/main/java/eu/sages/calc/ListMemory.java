package eu.sages.calc;

import java.util.ArrayList;
import java.util.List;

public class ListMemory implements Memory {

    private List<Double> values = new ArrayList<>();

    @Override
    public void setValue(double value) {
        values.add(value);
    }

    @Override
    public double getValue() {
        if(values.size()==0){
            return Double.NaN;
        } else {
            return values.get(values.size() - 1);
        }
    }
}
