package eu.sages.calc;

import java.util.Arrays;

public class CalculatorApp {

    public static void main(String[] args) {
        System.out.println("CalculatorApp.main");

        Calculator calc1 = new AdvancedCalculator(2);

        calc1.add(2, 3,4,5);
        calc1.multiply(4);
        calc1.subtract(0.123);

        calc1.display();


        Calculator calc2 = new AdvancedCalculator();
        calc2.add(4);
        calc2.multiply(4);

        // casting
        if(calc2 instanceof AdvancedCalculator) {
            ((AdvancedCalculator) calc2).power(3);
        } else{

        }

        try {
            calc2.divide(1);
        } catch (CalculatorException e) {
            e.printStackTrace();
        }


        System.out.println("calc2 value: " + calc2.getValue());


        int[] array = calc1.enhance(1,2,3,4,5);
        System.out.println("enhanced: " + Arrays.toString(array));



    }
}
