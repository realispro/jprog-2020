package eu.sages.calc;

public abstract class Calculator {

    protected Memory memory = new ListMemory();

    public Calculator(double value){
        super(); // new Object();
        System.out.println("[calculator] parametrized cnstructor");
        this.memory.setValue(value);
    }

    /*public Calculator(){
        this(0);
        System.out.println("[calculator] default constructor");
    }*/

    public abstract void display();

    public double add(double x, double... operands){
        this.memory.setValue( this.memory.getValue() + x );
        for(double o : operands){
            this.memory.setValue( this.memory.getValue() + o );
        }
        return this.memory.getValue();
    }

    public double subtract(double operand){
        this.memory.setValue( this.memory.getValue() - operand );
        return this.memory.getValue();
    }

    public double divide(double operand) throws CalculatorException {
        this.memory.setValue( this.memory.getValue() / operand );
        return this.memory.getValue();
    }

    public double multiply(double operand){
        this.memory.setValue( this.memory.getValue() * operand );
        return this.memory.getValue();
    }

    public static int multiply(int operand1, int operand2){
        return operand1*operand2;
    }

    public double getValue() {
        return this.memory.getValue();
    }


    public int[] enhance(int... array){
        for(int i=0; i<array.length; i++){
            array[i] = array[i]*2;
        }
        return array;
    }

    @Override
    public String toString() {
        return "Calculator{" +
                "memory=" + memory +
                '}';
    }
}
