package eu.sages.calc;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class AdvancedCalculator extends Calculator {

    public final static int STATE_VERSION = 70_000; // 0, 0.0, false, null

    public AdvancedCalculator(double value) {
        super(value);

    }

    @Override
    public void display() {
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.set(2020,10,25, 10,52, 0);
        calendar.add(Calendar.MINUTE, 7);
        date = calendar.getTime();

        Locale locale = new Locale("pl", "PL");
        Locale.setDefault(locale);
        DateFormat format = new SimpleDateFormat("yyyy$MM$dd hh$mm$ss z G");
                //DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.FULL, locale);

        // java.time
        LocalDateTime localDateTime = LocalDateTime.now();
                //LocalDateTime.of(2020, Month.NOVEMBER,25, 11,24);
        LocalDate localDate = LocalDate.now().plusDays(7);
        LocalTime localTime = LocalTime.now().plusHours(5);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh-mm-ss a z G", locale);

        ZoneId warsawZone = ZoneId.of("Europe/Warsaw");
        ZonedDateTime warsawDateTime = localDateTime.atZone(warsawZone);

        ZoneId singaporeZone = ZoneId.of("Asia/Singapore");
        ZonedDateTime singaporeDateTime = warsawDateTime.withZoneSameInstant(singaporeZone).plusHours(9);

        String timestamp = singaporeDateTime.format(formatter);

        System.out.println("[" + timestamp + "] calculator value: " + this.memory.getValue());
    }

    public AdvancedCalculator(){
        this(0);
    }

    public double power(int operand){
        this.memory.setValue(Math.pow(this.memory.getValue(), operand));
        return this.memory.getValue();
    }

    @Override
    public double divide(double operand) throws CalculatorException {
        if(operand==0){
            throw new CalculatorException("pamietaj cholero nie dziel przez zero");
        }
        return super.divide(operand);
    }
}
