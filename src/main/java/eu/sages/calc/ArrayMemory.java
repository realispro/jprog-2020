package eu.sages.calc;

import java.util.Arrays;

public class ArrayMemory implements Memory {

    private double[] values;

    private int index = 0;

    public ArrayMemory(int size){
        values = new double[size];
    }

    @Override
    public void setValue(double value) {
        //     [0,1,2,3,4] + 5
        // v1: [5,1,2,3,4] index: 0
        // v2: [0,1,2,3,4,5] index: 5
        // v3: [1,2,3,4,5] index: 4 !!!

        if(index==values.length-1){
            System.arraycopy(values, 1, values, 0, values.length-1);
            // [1,2,3,4,4]
        } else {
            index++;
        }
        values[index] = value;
    }

    @Override
    public double getValue() {
        return values[index];
    }

    @Override
    public String toString() {
        return "ArrayMemory{" +
                "values=" + Arrays.toString(values) +
                ", index=" + index +
                '}';
    }
}
