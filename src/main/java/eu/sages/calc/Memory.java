package eu.sages.calc;

/**
 * Interface representing memory
 */
public interface Memory {

    /**
     * adds provided value to memory, depending on an implementation
     * @param value value to add
     */
    void setValue(double value);

    /**
     * Provides the latest value from memory.
     * Does not remove it from internal data structure.
     * @return current value or NaN if missing
     */
    double getValue();

}
