package eu.sages.zoo;

public abstract class Bear extends Animal{

    public Bear(int size, String name) {
        super(size, name);
    }

    @Override
    public void move() {
        System.out.println("bear is walking slowly");
    }
}
