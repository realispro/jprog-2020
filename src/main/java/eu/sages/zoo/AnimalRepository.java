package eu.sages.zoo;

import java.util.List;
import java.util.Optional;

// Repository, Data Access Object = DAO
public interface AnimalRepository {

    List<Animal> findAll();

    void add(Animal animal);

    Optional<Animal> findByName(String name);

}
