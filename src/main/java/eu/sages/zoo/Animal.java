package eu.sages.zoo;

import java.util.Objects;

public abstract class Animal implements Comparable<Animal>{

    private int size;

    private String name;

    public Animal(int size, String name) {
        this.size = size;
        this.name = name;
    }

    public void eat(String food){
        System.out.println(name + " is eating " + food);
    }

    public String getDescription(){
        return "just an animal";
    }

    public abstract void move();

    public int getSize() {
        return size;
    }

    public String getName() {
        return name;
    }

    @Override
    public int compareTo(Animal a) {
        return a.name.compareTo(this.name);
                //this.size - a.getSize();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Animal animal = (Animal) o;
        return size == animal.size &&
                name.equals(animal.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(size, name);
    }

    @Override
    public String toString() {
        return "Animal{" +
                "size=" + size +
                ", name='" + name + '\'' +
                '}';
    }
}
