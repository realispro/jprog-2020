package eu.sages.zoo;

import java.util.*;

public class ZooApp {

    public static void main(String[] args) {
        System.out.println("ZooApp.main");

        Animal animal = getAnimal();

        Collection collection = new ArrayList<>();
        collection.add(new PolarBear(1000, "Jurij"));
        collection.add(animal);
        collection.add(new Grizzli(1001, "Donald"));
        collection.add(new Camel(400, "Ahmed"));
        collection.add(new Shark(300, "Steven"));
        collection.add(new String("Reksio"));
        collection.add(animal);

        List<Animal> animals = new ArrayList<>();
        for( Object o : collection ){
            if(o instanceof Animal){
                animals.add((Animal)o);
            }
        }
        System.out.println("animal included: " + animals.contains(animal));

        Comparator<Animal> animalComparator = new AnimalComparator();
        Collections.sort(animals, animalComparator); // TimSort
        System.out.println("animals = " + animals);

        animal.eat("szprotka");
        animal.move();

        Map<Animal, String> map = new HashMap<>();
        map.put(animal, "Marcin");
        map.put(new PolarBear(1000, "Jurij"), "Agnieszka");
        map.put(new Camel(130, "Ahmed"), "Bartosz");
        map.put(new Shark(300, "Steven"), "Krzysztof");
        map.put(new Shark(330, "Joe"), "Pawel");
        map.put(animal, "Bartosz");

        System.out.println("map: " + map);

        String careTaker = map.get(new Camel(130, "Ahmed"));
        System.out.println("careTaker of " + animal.getName() + " is " + careTaker);

    }

    public static Animal getAnimal(){
        return //new Shark(500, "Steven");
                new Grizzli(1200, "Wojtek");
    }
}
