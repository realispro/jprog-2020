package eu.sages.zoo;

public class Shark extends Animal{

    public Shark(int size, String name) {
        super(size, name);
    }

    @Override
    public void move() {
        System.out.println("shark is swimming very fast");
    }

    @Override
    public void eat(String food) {
        if(food.equalsIgnoreCase("OWIES")){
            System.out.println("tfu.....");
        } else {
            super.eat(food);
        }
    }
}
