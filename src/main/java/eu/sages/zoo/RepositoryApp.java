package eu.sages.zoo;

import java.util.List;
import java.util.Optional;

public class RepositoryApp {

    public static void main(String[] args) {
        System.out.println("RepositoryApp.main");

        AnimalRepository repository = new DatabaseAnimalRepository();
                //new FileAnimalRepository();

        List<Animal> animals = repository.findAll();
        animals.forEach(a-> System.out.println(a));

        Animal a = new Camel(1, "George");
        repository.add(a);

        Optional<Animal> jurij = repository.findByName("Kasia");
        if(jurij.isPresent()){
            System.out.println("Jurij found: " + jurij.get());
        } else {
            System.out.println("Jurij not found :(");
        }

        System.out.println("done.");
    }
}
