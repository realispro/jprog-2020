package eu.sages.zoo;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamApp {

    public static void main(String[] args) {

        Animal animal = new Grizzli(800, "Donald");
        List<Animal> animals = new ArrayList<>();
        animals.add(animal);
        animals.add(new PolarBear(1000, "Wladimir"));
        animals.add(animal);
        animals.add(new Shark(300, "Steven"));
        animals.add(new Camel(200, "Ahmed"));


        //Collections.sort(animals, animalComparator);
        //animals.sort((a1, a2) -> a1.getSize()-a2.getSize());
        //animals.removeIf((a) -> a.getSize()>=1000);
        //animals.forEach(a -> System.out.println("name: " + a.getName()));
        
        // Stream API
        //long count =
        //List<Animal> processedAnimals =
        //boolean match =
        Optional<Animal> optionalAnimal =
        //List<String> names =
             animals.stream()
                .distinct()
                .sorted((a1,a2)->a1.getSize()-a2.getSize()) // Comparator (1.2)
                .filter((a) -> a.getSize()<1000)            // Predicate  (1.8)
                .peek(a-> System.out.println("peek: " + a)) // Consumer   (1.8)
                //.map(a->a.getName())                      // Function   (1.8)
                //.collect(Collectors.toList());
                .findFirst();
                //.allMatch(a->a.getSize()<300);
                //.collect(Collectors.toList());
                //.count();

        //System.out.println("names = " + names);
        Supplier<Animal> animalSupplier = () -> new Camel(1, "George");
        Animal a = optionalAnimal.orElse(new Camel(1, "George"));
                //optionalAnimal.orElseGet(animalSupplier);
        System.out.println("a from optional:" + a);
        //System.out.println("match = " + match);
        //System.out.println("processedAnimals = " + processedAnimals);
        //System.out.println("count = " + count);
        


        System.out.println("animals = " + animals);
    }

}
