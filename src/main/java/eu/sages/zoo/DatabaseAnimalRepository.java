package eu.sages.zoo;

import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Properties;

public class DatabaseAnimalRepository implements AnimalRepository{

    private String driverClass;
    private String url;
    private String user;
    private String password;

    public DatabaseAnimalRepository(){
        Properties props = new Properties();
        try {
            props.load(DatabaseAnimalRepository.class.getResourceAsStream("/db.properties"));
            driverClass = props.getProperty("db.driver");
            url = props.getProperty("db.url");
            user = props.getProperty("db.user");
            password = props.getProperty("db.password");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Animal> findAll() {

        List<Animal> animals = new ArrayList<>();

        try(Connection connection = getConnection();){
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("select type, name, size from animal");
            while(rs.next()){
                String type = rs.getString("type");
                String name = rs.getString("name");
                int size = rs.getInt("size");
                animals.add(createAnimal(type,name,size));
            }
        } catch (SQLException e) {
            throw new RuntimeException("problem while fetching all animals", e);
        }
        return animals;
    }

    @Override
    public void add(Animal animal) {

        try(Connection connection = getConnection();){
            connection.setAutoCommit(false);
            PreparedStatement stmt = connection
                    .prepareStatement("INSERT INTO ANIMAL(TYPE,NAME,SIZE) VALUES (?, ?, ?)");
            stmt.setString(1, animal.getClass().getSimpleName());
            stmt.setString(2, animal.getName());
            stmt.setInt(3,animal.getSize());
            int count = stmt.executeUpdate();
            System.out.println(count + " animal(s) updated");
            connection.commit();
        } catch (SQLException e) {
            throw new RuntimeException("problem while inserting animal", e);
        }
    }

    @Override
    public Optional<Animal> findByName(String n) {

        try(Connection connection = getConnection()){
            PreparedStatement stmt =
                    connection.prepareStatement("select type, name, size from animal where name=?");
            stmt.setString(1,n);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                String type = rs.getString("type");
                String name = rs.getString("name");
                int size = rs.getInt("size");
                Animal animal = createAnimal(type, name, size);
                return Optional.of(animal);
            }
        } catch (SQLException e) {
            throw new RuntimeException("problem while searching animal " + n, e);
        }

        return Optional.empty();
    }

    private Connection getConnection(){
        try {
            Class.forName(driverClass);
            return DriverManager.getConnection(url, user, password);
        } catch (ClassNotFoundException | SQLException e) {
            throw new RuntimeException("exception while obtaining db connection", e);
        }
    }

    private Animal createAnimal(String type, String name, int size){

        try {
            Class clazz = Class.forName("eu.sages.zoo." + type);
            Constructor constructor = clazz.getDeclaredConstructor(int.class, String.class);
            Object o = constructor.newInstance(size, name);
            return (Animal) o;
        } catch (Exception e) {
            throw new RuntimeException("animal class not found", e);
        }

    }
}
