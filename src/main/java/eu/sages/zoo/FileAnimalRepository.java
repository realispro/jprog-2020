package eu.sages.zoo;

import java.io.*;
import java.lang.reflect.Constructor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class FileAnimalRepository implements AnimalRepository{

    private File file;

    public FileAnimalRepository() {
        try {
            File dir = new File("target" + File.separator + "repository");
            if (!dir.exists()) {
                System.out.println("creating directory " + dir.getAbsolutePath());
                dir.mkdirs();
            } else {
                System.out.println("directory " + dir.getAbsolutePath() + " already exists");
                for(File file : dir.listFiles((dir1, name) -> name.endsWith(".repo"))){
                    System.out.println("file: " + file.getAbsolutePath());
                }
            }
            file = new File(dir, "animal.repo");
            if (file.exists()) {
                System.out.println("Repository file: " + file.getAbsolutePath() + " already exists");
            } else {
                System.out.println("creating repository file " + file.getAbsolutePath());
                file.createNewFile();
            }
        }catch(IOException e){
            throw new RuntimeException("file animal repository initialization failure", e);
        }


    }


    @Override
    public List<Animal> findAll() {

        List<Animal> animals = new ArrayList<>();
        // try-with-resources
        try(BufferedReader br = new BufferedReader(new FileReader(file));) {
            String line;
            while((line=br.readLine())!=null){
                String[] cells = line.split(";");
                String type = cells[0];
                String name = cells[1];
                int size = Integer.parseInt(cells[2]);
                animals.add(createAnimal(type, name, size));
                System.out.println("name = " + name);
            }

        }catch(IOException e){
            e.printStackTrace();
        }
        return animals;
    }

    private Animal createAnimal(String type, String name, int size){

        try {
            Class clazz = Class.forName("eu.sages.zoo." + type);
            Constructor constructor = clazz.getDeclaredConstructor(int.class, String.class);
            Object o = constructor.newInstance(size, name);
            return (Animal) o;
        } catch (Exception e) {
            throw new RuntimeException("animal class not found", e);
        }

    }

    @Override
    public void add(Animal animal) {

        try(BufferedWriter bw = new BufferedWriter(new FileWriter(file, true))){
            String type = animal.getClass().getSimpleName();
            String name = animal.getName();
            int size = animal.getSize();
            bw.write(type + ";" + name + ";" + size + ";\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Optional<Animal> findByName(String name) {

        Path path = Paths.get("target","repository", "animal.repo");
        try {
            return  Files.lines(path)
                    .filter(row-> row.split(";")[1].equalsIgnoreCase(name))
                    .map(line->{
                        String[] cells = line.split(";");
                        return createAnimal(cells[0], cells[1], Integer.parseInt(cells[2]));
                    })
                    .findFirst();

        } catch (IOException e) {
            throw new RuntimeException("exception while searching for an animal " + name, e);
        }

    }
}
