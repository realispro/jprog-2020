package eu.sages.zoo;

import eu.sages.travel.Transportation;

import java.io.Serializable;

public class Camel extends Animal implements Transportation, Serializable {


    public Camel(int size, String name) {
        super(size, name);
    }

    @Override
    public void move() {
        System.out.println("camel is walking yet slower than bear");
    }

    @Override
    public void transport(String passenger) {
        System.out.println("loading passenger " + passenger);
        move();
    }

    @Override
    public String getDescription() {
        return Transportation.super.getDescription();
    }
}
