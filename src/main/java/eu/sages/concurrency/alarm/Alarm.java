package eu.sages.concurrency.alarm;



import eu.sages.concurrency.ThreadNamePrefixPrintStream;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Alarm {

    public static void main(String[] args) {

        System.setOut(new ThreadNamePrefixPrintStream(System.out));

        ExecutorService es = Executors.newFixedThreadPool(2);

        Beeper b = new Beeper();
        Light l = new Light();

        es.execute(b);
        es.execute(l);
        es.shutdown();



        System.out.println("done.");
    }
}
