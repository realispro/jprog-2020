package eu.sages.concurrency.charger;


import eu.sages.concurrency.ThreadNamePrefixPrintStream;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ChargerStarter {

    public static void main(String[] args) {

        System.setOut(new ThreadNamePrefixPrintStream(System.out));

        Electricity electricity = Electricity.getInstance();
        electricity.turnOn();

        ExecutorService es = Executors.newFixedThreadPool(2);

        Charger samsungCharger = new Charger();
        Charger appleCharger = new Charger();

        Phone samsung = new Phone(50, "Samsung S11");
        Phone apple = new Phone(35, "Iphone XR");

        es.execute(() -> samsungCharger.chargeDevice(samsung, 5));
        es.execute(() -> appleCharger.chargeDevice(apple, 7));

        es.shutdown();

        try {
            Thread.sleep(15*1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        electricity.turnOff();

        System.out.println("done.");



    }
}
