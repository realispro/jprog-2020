package eu.sages.concurrency.account;

public class DepositTask implements Runnable {

    private Account account;

    public DepositTask(Account account) {
        this.account = account;
    }

    @Override
    public void run() {
        for(int i=0; i<1_000_000; i++){
            account.deposit(1);
        }
        System.out.println("after deposit: " + account.getBalance());
    }
}
