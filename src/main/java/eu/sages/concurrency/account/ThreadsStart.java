package eu.sages.concurrency.account;


import eu.sages.concurrency.ThreadNamePrefixPrintStream;

import java.util.Locale;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadsStart {

    public static void main(String[] args) {

        System.setOut(new ThreadNamePrefixPrintStream(System.out));

        Locale locale = new Locale("pl");
        ResourceBundle bundle = ResourceBundle.getBundle("messages", locale);
        // messages_ru
        // messages_default
        // messages

        ExecutorService es = Executors.newFixedThreadPool(2);

        System.out.println("ThreadsStart.main");
        Account account = new Account();

        es.execute(new DepositTask(account));
        es.execute(new WithdrawTask(account));

        // I18N - INTERNATIONALIZATION
        System.out.println( bundle.getString("account.balance") + " = " + account.getBalance());

        es.shutdown();
        System.out.println(bundle.getString("account.done"));


    }

}
