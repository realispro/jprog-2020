package eu.sages.concurrency.account;


import java.util.concurrent.atomic.AtomicInteger;

public class Account {

    private AtomicInteger balance = new AtomicInteger(1_000_000);


    public /*synchronized*/ boolean withdraw(int amount) {

            if (balance.get() >= amount) {
                balance.addAndGet(-amount);
                return true;
            } else {
                return false;
            }

    }

    public /*synchronized*/ void deposit(int amount) {

        //synchronized (this) {
            balance.addAndGet(amount);
        //}

    }

    public int getBalance() {
        return balance.get();
    }
}
