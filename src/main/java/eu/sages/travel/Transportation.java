package eu.sages.travel;

@FunctionalInterface
public interface Transportation {

    void transport(String passenger);


    // java 8
    default String getDescription(){
        return "not provided";
    }

}
