package eu.sages.travel;

import java.io.Serializable;

public interface SpeedTransportation extends Transportation, Serializable {

    int getSpeed();

}
