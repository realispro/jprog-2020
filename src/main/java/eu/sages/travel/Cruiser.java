package eu.sages.travel;

public class Cruiser implements Transportation{

    @Override
    public void transport(String passenger) {
        System.out.println("passenger " + passenger + " is cruising");
    }

    @Override
    public String getDescription() {
        return "water transportation";
    }
}
