package eu.sages;

public enum WeekDay {

    MONDAY(false),
    TUESDAY(false),
    WEDNESDAY(false),
    THURSDAY(false),
    FRIDAY(true),
    SATURDAY(true),
    SUNDAY(true);

    public boolean weekend = false;

    boolean monday = false;

    WeekDay(boolean weekend){
        this.weekend = weekend;
    }
}
